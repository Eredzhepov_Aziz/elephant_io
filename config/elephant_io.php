<?php

	return [
			'host'    => '127.0.0.1',
			'port'    => 3000,
			'options' => [
					'context' => [],
					'debug'   => FALSE,
					'wait'    => 100000,
					'timeout' => 60
			]
	];

