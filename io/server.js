var $app = require('express')(),
	$server = require('http').Server($app),
	$io = require('socket.io')($server);

$io.on('connection', function ($socket) {
	console.log('New client connected');

	$socket.on('create_room', function ($room) {
		$socket.join($room);
		console.log('Create Room', $room);
	});

	$socket.on('event', function ($data) {
		$io.to($data.room).emit('event', $data);
		console.log('New event in room', $data.room);
	});
});

$io.on('disconnect', function ($socket) {
	console.log('Client disconnected');
});

$server.listen(3000);