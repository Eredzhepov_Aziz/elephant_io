<?php

	namespace Oleus\ElephantIO;

	use Oleus\ElephantIO\Library\Client;
	use Oleus\ElephantIO\Library\Engine\SocketIO\Version2X;
	use Oleus\ElephantIO\Library\Exception\ServerConnectionFailureException;

	class ElephantEmitter
	{

		/**
		 * @var Client
		 */
		public $client;

		/**
		 * Emitter constructor.
		 */
		public function __construct()
		{
			$engine = new Version2X(config('elephant_io.host') . ':' . config('elephant_io.port'), config('elephant_io.options'));
			$this->client = new Client($engine);
		}

		/**
		 * Emit in socket.io
		 * @param $event - название события
		 * @param $room - комната в которую надо отпраить данные
		 * @param $data - данные для передачи
		 * @throws \Exception
		 */
		public function emit($event, $room, $data)
		{
			try {
				$data = array_merge(array('room' => $room), $data);
				$this->client->initialize();
				$this->client->emit($event, $data);
				$this->client->close();
			} catch(ServerConnectionFailureException $exception) {
				echo 'Server Connection Failure!!!';
				throw $exception;
			}
		}

	}