<?php

	namespace Oleus\ElephantIO;

	use Illuminate\Support\ServiceProvider;
	use Oleus\ElephantIO\Facades\ElephantIO;

	class ElephantServiceProvider extends ServiceProvider
	{

		protected $defer = FALSE;

		/**
		 *
		 */
		public function boot()
		{
			// публикуем настройки
			$this->publishes([
					__DIR__ . '/../config/elephant_io.php' => config_path('elephant_io.php')
			], 'elephant.io');

			// публикуем файл IO сервера
			$this->publishes([
					__DIR__ . '/../io/server.js' => base_path('io/server.js')
			], 'elephant.io');
		}

		/**
		 *
		 */
		public function register()
		{
			$this->app->singleton(ElephantEmitter::class, function ($app) {
				return new ElephantEmitter;
			});

			$this->app->alias(ElephantEmitter::class, 'ElephantIO');
		}

	}