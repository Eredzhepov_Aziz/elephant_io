<?php

	namespace Oleus\ElephantIO\Facades;

	use Illuminate\Support\Facades\Facade;

	class ElephantIO extends Facade
	{

		protected static function getFacadeAccessor()
		{
			return 'ElephantIO';
		}

	}