<?php

	namespace Oleus\ElephantIO\Library;

	use Psr\Log\NullLogger;
	use Psr\Log\LoggerInterface;
	use Oleus\ElephantIO\Library\Exception\SocketException;

	class Client
	{
		/** @var EngineInterface */
		private $engine;

		/** @var LoggerInterface */
		private $logger;

		private $isConnected = FALSE;

		public function __construct(EngineInterface $engine, LoggerInterface $logger = NULL)
		{
			$this->engine = $engine;
			$this->logger = $logger ? : new NullLogger;
		}

		public function __destruct()
		{
			if(!$this->isConnected) {
				return;
			}

			$this->close();
		}

		/**
		 * Connects to the websocket
		 * @param boolean $keepAlive keep alive the connection (not supported yet) ?
		 * @return $this
		 */
		public function initialize($keepAlive = FALSE)
		{
			try {
				$this->logger->debug('Connecting to the websocket');
				$this->engine->connect();
				$this->logger->debug('Connected to the server');
				$this->isConnected = TRUE;

				if(TRUE === $keepAlive) {
					$this->logger->debug('Keeping alive the connection to the websocket');
					$this->engine->keepAlive();
				}
			} catch(SocketException $e) {
				$this->logger->error('Could not connect to the server', ['exception' => $e]);

				throw $e;
			}

			return $this;
		}

		/**
		 * Reads a message from the socket
		 * @return string Message read from the socket
		 */
		public function read()
		{
			$this->logger->debug('Reading a new message from the socket');

			return $this->engine->read();
		}

		/**
		 * Emits a message through the engine
		 * @param string $event
		 * @param array  $args
		 * @return $this
		 */
		public function emit($event, array $args)
		{
			$this->logger->debug('Sending a new message', ['event' => $event,
			                                               'args'  => $args]);
			$this->engine->emit($event, $args);

			return $this;
		}

		/**
		 * Sets the namespace for the next messages
		 * @param string namespace the name of the namespace
		 * @return $this
		 */
		public function of($namespace)
		{
			$this->logger->debug('Setting the namespace', ['namespace' => $namespace]);
			$this->engine->of($namespace);

			return $this;
		}

		/**
		 * Closes the connection
		 * @return $this
		 */
		public function close()
		{
			$this->logger->debug('Closing the connection to the websocket');
			$this->engine->close();

			$this->isConnected = FALSE;

			return $this;
		}

		/**
		 * Gets the engine used, for more advanced functions
		 * @return EngineInterface
		 */
		public function getEngine()
		{
			return $this->engine;
		}
	}

