<?php

	namespace Oleus\ElephantIO\Library\Engine\SocketIO;

	use DomainException;
	use InvalidArgumentException;
	use UnexpectedValueException;
	use Psr\Log\LoggerInterface;
	use Oleus\ElephantIO\Library\EngineInterface;
	use Oleus\ElephantIO\Library\Payload\Encoder;
	use Oleus\ElephantIO\Library\Engine\AbstractSocketIO;

	use Oleus\ElephantIO\Library\Exception\SocketException;
	use Oleus\ElephantIO\Library\Exception\UnsupportedTransportException;
	use Oleus\ElephantIO\Library\Exception\ServerConnectionFailureException;

	/**
	 * Implements the dialog with Socket.IO version 2.x
	 * Based on the work of Mathieu Lallemand (@lalmat)
	 * @author Baptiste Clavié <baptiste@wisembly.com>
	 * @link https://tools.ietf.org/html/rfc6455#section-5.2 Websocket's RFC
	 */
	class Version2X extends Version1X
	{

		/** {@inheritDoc} */
		public function getName()
		{
			return 'SocketIO Version 2.X';
		}

		/** {@inheritDoc} */
		protected function getDefaultOptions()
		{
			$defaults = parent::getDefaultOptions();

			$defaults['version'] = 3;

			return $defaults;
		}
	}

