<?php

	namespace Oleus\ElephantIO\Library\Exception;

	use Exception;
	use InvalidArgumentException;

	class MalformedUrlException extends InvalidArgumentException
	{
		public function __construct($url, Exception $previous = NULL)
		{
			parent::__construct(sprintf('The url "%s" seems to be malformed', $url), 0, $previous);
		}
	}

