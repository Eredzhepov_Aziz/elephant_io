<?php

	namespace Oleus\ElephantIO\Library\Exception;

	use Exception;
	use RuntimeException;

	class ServerConnectionFailureException extends RuntimeException
	{
		public function __construct(Exception $previous = NULL)
		{
			parent::__construct('An error occurred while trying to establish a connection to the server', 0, $previous);
		}
	}

