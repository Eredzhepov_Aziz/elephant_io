<?php

	namespace Oleus\ElephantIO\Library\Exception;

	use RuntimeException;

	class SocketException extends RuntimeException
	{
		public function __construct($errno, $error, \Exception $previous = NULL)
		{
			parent::__construct(sprintf('There was an error while attempting to open a connection to the socket (Err #%d : %s)', $errno, $error), $errno, $previous);
		}
	}

