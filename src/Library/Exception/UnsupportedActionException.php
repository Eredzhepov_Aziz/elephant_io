<?php

	namespace Oleus\ElephantIO\Library\Exception;

	use BadMethodCallException;
	use Oleus\ElephantIO\Library\EngineInterface;

	class UnsupportedActionException extends BadMethodCallException
	{
		public function __construct(EngineInterface $engine, $action, Exception $previous = NULL)
		{
			parent::__construct(sprintf('The action "%s" is not supported by the engine "%s"', $engine->getName(), $action), 0, $previous);
		}
	}

