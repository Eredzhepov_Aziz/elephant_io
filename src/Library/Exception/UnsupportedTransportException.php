<?php

	namespace Oleus\ElephantIO\Library\Exception;

	use RuntimeException;

	class UnsupportedTransportException extends RuntimeException
	{
		public function __construct($transport, \Exception $previous = NULL)
		{
			parent::__construct(sprintf('This server does not support the %s transport, aborting', $transport), 0, $previous);
		}
	}

